import {ReactComponent as GitLabIcon} from 'assets/img/gitlab_logo.svg' 
import './styles.css';

function Navbar() {
   
    return (
        <header>
        <nav className='container'>
            <div className='dsmovie-nav-content'>
                <h1>DSMovie</h1>
                <a href="https://gitlab.com/luizolivas/dsfilmes" target="_blank" rel="noopener noreferrer">
                    <div className='dsmovie-contact-container'>
                        <GitLabIcon />
                        <p className='dsmovie-contact-link'>/luizolivas</p>
                    </div>
                </a>
            </div>
        </nav>
        </header>
    )
}

export default Navbar;